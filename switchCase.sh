

# The case statement is useful and processes faster than an else-if ladder. Instead of checking all if-else conditions, the case statement directly select the block to execute based on an input.

# read -p "Enter your choice [yes/no]:" choice
 
# case $choice in
#      yes)
#           echo "Thank you"
#           echo "Your type: Yes"
#           ;;
#      no)
#           echo "Ooops"
#           echo "You type: No"
#           ;;
#      *)
#           echo "Sorry, invalid input"
#           ;;
# esac



echo "=============================================================================="

# Multple Strings in Case Options

# read -p "Enter your choice [yes/no]:" choice1
# echo "++++++ You entered $choice1"
# case $choice1 in
#      Y/y/Yes/YES/yes) # doesn't work only matches the whole string Y/y/Yes/YES/yes
#           echo "Thank you"
#           echo "Your type: Yes"
#           ;;
#      N/n/No/NO/no)
#           echo "Ooops"
#           echo "You type: No"
#           ;;
#      *)
#           echo "Sorry, invalid input"
#           ;;
# esac


echo "================================================================================"
# You can use wildcard characters like *,? and [] with the case statement. But still, some of the braces expansion still not work. Now, you can set shopt -s extglob to use extended pattern matching.

read -p "Enter a string:" choice
shopt -s extglob
case $choice in
     a*)                    ### matches anything starting with "a"
          echo "starts with a"
          ;;
 
     b?)                    ### matches any two-character string starting with "b"
          echo " 2 char sting start with b"
          ;;
 
     s[td])                 ### matches "st" or "sd"
          echo " st OR sd"
          ;;
 
     r[ao]m)                ### matches "ram" or "rom"
          echo "ram rom"
          ;;
 
     me?(e)t)               ### matches "met" or "meet"
         echo "matches met or meet"
          ;;
 
     @(a|e|i|o|u))          ### matches one vowel
         echo "matches vowels Multple char"
          ;;
 
     *)                     ### Catchall matches anything not matched above
         echo "Doesnt match any case"
          ;; 
esac

