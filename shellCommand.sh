# Basically, a shell script is a collection of shell commands. You can simply run any bash shell command inside a shell script. In this tutorial, you will find some special ways to execute shell commands in a bash script to archive specific tasks.


VAR=`date +"%d%b%Y"`
echo $VAR


VAR=`date +"%d%b%Y"`
mkdir /backup/db/$VAR

#  OR using single command
mkdir /backup/db/`date +"%d%b%Y"`

