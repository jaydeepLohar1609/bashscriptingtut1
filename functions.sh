functionName()
{
    echo ' 1st function called'
}
functionName


# The passing argument to functions is similar to pass an argument to command from shell. Functions receives arguments to $1,$2… etc. Create a shell script using following code.

functionName1()
{
    echo " 1st arg to function is $1 ------ 2nd arg is $2 ---------- 3rd ard is $3"
    echo " Total arg passed to function is $#"
}
functionName1 hello_World 100 Code

echo " 1st cmd line arg to file is $1 ------ 2nd arg is $2 ---------- 3rd ard is $3"
echo " Total cmd line arg passed to file is $#"