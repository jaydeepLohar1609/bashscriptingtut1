#     *
#    ***
#   *****
#  *******
# *********
# number of pyramid rows inputted by user
echoSpace()
{
    echo $((" "*$1))
}
pattern1()
{
    pyramidHeight=$1
    for((i=1;i<=$pyramidHeight;i++))
    do
        # echo "--------$(($pyramidHeight-$i))"
        for((j=1;j<=$(($pyramidHeight-$i));j++))
        do
            printf " "
        done
        for((k=1;k<=$((2*$i-1));k++))
        do
            printf "*"
        done
        printf "\n"
    done
}
read -p "Enter the pyramid height " num
pattern1 $num