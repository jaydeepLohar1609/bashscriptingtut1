
for i in 1 2 3 4 5
do
   echo "$i"
done


for i in {1..5}
do
   echo "$i"
done


for day in SUN MON TUE WED THU FRI SUN
do
   echo "$day"
done


#2. Bash – For Loop in C Style
for ((i=1; i<=10; i++))
do
  echo "$i"
done

#3. Bash – For Loop with Files
# You can access filenames one by one in for loop under the specified directory. For example, read all files from the current directory.
for fname in *
do
  ls -l $fname
done