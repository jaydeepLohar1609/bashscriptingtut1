# adb kill-server && adb tcpip 5555 && adb connect 192.168.1.128

# dpkg command tutorial


#pass package name as command line argument and specific flag to perform action on the packageName
dpkg -i packageName.deb


# list all packages
dpkg -l


# To remove the “.deb” package, we must specify the package name “flashpluginnonfree“, not the original name “flashplugin-nonfree_3.2_i386.deb“. The “-r” option is used to remove/uninstall a package.
dpkg -r anydesk


# You can also use ‘p‘ option in place of ‘r’ which will remove the package along with configuration file. The ‘r‘ option will only remove the package and not configuration files.
dpkg -p anydesk


# To view the content of a particular package, use the “-c” option as shown. The command will display the contents of a “.deb” package in long-list format.
# here we have to provide the full package name
dpkg -c anydesk_5.5.4-1_amd64.deb


# Using “-s” option with package name, will display whether an deb package installed or not.
dpkg -s anydesk


# Recursively, install all the regular files matching pattern “*.deb” found at specified directories and all of its subdirectories. This can be used with “-R” and “–install” options. For example, I will install all the “.deb” packages from the directory called “debpackages“.
dpkg -R --install debpackages/


# Using action “–unpack” will unpack the package, but it will don’t install or configure it.
dpkg --unpack flashplugin-nonfree_3.2_i386.deb


# The option “–configure” will reconfigure a already unpacked package.
[root@tecmint~]# dpkg --configure flashplugin-nonfree
Setting up flashplugin-nonfree (1:3.2) ...



# ====================================================================================================================================================

# xargs command
# xargs is a Unix command which can be used to build and execute commands from standard input.
# Importance :
# Some commands like grep can accept input as parameters, but some commands accepts arguments, this is place where xargs came into picture.

xargs [options] [command]
# xargs options :
# -0 : input items are terminated by null character instead of white spaces
# -a file : read items from file instead of standard input
# –delimiter = delim : input items are terminated by a special character
# -E eof-str : set the end of file string to eof-str
# -I replace-str : replace occurrences of replace-str in the initial arguments with names read from standard input
# -L max-lines : use at-most max-lines non-blank input lines per command line.
# -p : prompt the user about whether to run each command line and read a line from terminal.
# -r : If the standard input does not contain any nonblanks, do not run the command
# -x : exit if the size is exceeded.
# –help : print the summary of options to xargs and exit
# –version : print the version no. of xargs and exit


#--------------------- android ADB
# mido:/ $ xargs --version
# usage: xargs [-ptxr0] [-s NUM] [-n NUM] [-L NUM] [-E STR] COMMAND...

# Run command line one or more times, appending arguments from stdin.

# If command exits with 255, don't launch another even if arguments remain.

# -s	Size in bytes per command line
# -n	Max number of arguments per command
# -0	Each argument is NULL terminated, no whitespace or quote processing
# #-p	Prompt for y/n from tty before running each command
# #-t	Trace, print command line to stderr
# #-x	Exit if can't fit everything in one command
# #-r	Don't run command with empty input
# #-L	Max number of lines of input per command
# -E	stop at line matching string


# ls -aR | grep -i 'modi' | sed  -e 's/ /\\ /g' | tr '\n' ' ' | xargs  mv -v -t /media/disk1/Modiji/
For example,

-t, --target-directory=DIRECTORY  move all SOURCE arguments into DIRECTORY

tr -s '\n'
will squeeze all consecutive newlines from the input into single newlines.

To characterize the three tools crudely:

tr works on characters (changes or deletes them).
sed works on lines (modifies words or other parts of lines, or inserts or deletes lines).
awk work on records with fields (by default whitespace separated fields on a line, but this may be changed by setting FS and RS).

curl
wget
wc
cat
-exec

The echo command, rigorously following the Unix philosophy of "do one thing, and do it well",


How to redirect into a file:

echo "Hello World" > file.txt
echo "Some More Text " >> file.txt
cat file.txt
---------------------->
Hello World
Some More Text

 echo "Pipes and arrows hey" "hello" "temp" | wc
   1       6      32

The program unzip is another such example. It'd be nice to curl down a zip file and pass it straight to unzip, which would bypass the need to save the zip file:

user@host:~$ curl http://example.com/some.zip | unzip


https://youtu.be/VgbnndezHbw

ls -t #list on basis of modification time newest

ls -l | less
| pg
| more

-------------------------------------------------------------------------------------------
grep command
-v Shows all lines that do not matched the search string
-c display only the count of matching lines
-n Show the matching lines and its number
-i match both upper & lower case
-l  Show just the name of the file with the string

---------------------------------------------------------------------------------------
sort command

-r Reverse sorting
-n Sorts numerically
-f Case insensitive sorting

----------------------------------------------------------------------------------------

cat fruits | grep -v a | sort -r
find .  -type f -print | wc -l 

# prints number of output lines from the previos command output..
And it search recursively in the whole sub directory
find .  -type f -print | head | wc -l 


 find .  -type f -print | head -n 3 |  sed 's/ /\\ /g' | xargs -0 basename | xargs -0 readlink -f

 ------------------------------------------------------------------------------------
 xargs command

 Well, before jumping onto its usage, it's important to understand what exactly Xargs does. In layman's terms, the tool - in its most basic form - reads data from standard input (stdin) and executes the command (supplied to it as an argument) one or more times based on the input read. Any blanks and spaces in the input are treated as delimiters, while blank lines are ignored. 

 While echo is the default command xargs executes, you can explicitly specify any other command. For example, you can pass the find command along with its '-name' option as an argument to xargs, and then pass the name of the file (or type of files) you want to find to search as input through stdin.


find .  -type f -print | head -n 3 |  sed 's/ /\\ /g' | tr '\n' ' '


youtube-dl -F https://www.youtube.com/watch?v=IH3pel72sWQ | grep best | cut -d ' ' -f 1 | xargs -i youtube-dl -f {} https://www.youtube.com/watch?v=IH3pel72sWQ


ls -a | grep -i 'dhoni ' | xargs -0i vlc {}
# using -0 results error in file name too long
# [00007fd0e4002190] filesystem stream error: cannot open file /media/disk1/dwayneJohnson/Dheere Dheere -  'Saibo' - Shor in the City feat Sakshi Singh Rawat-fXMQMPK7iao.mp4
dhoni
MS Dhoni & Sachin's Funny Moments At ISL Match With Bollywood Celebs-tokj3GJTwhE.mp4
Virat Kohli's CUTE Video With MS Dhoni's Daughter Zeva-7jvyF21dMLI.mp4
 (No such file or directory)
takes all files name and append it to other file name string..
# -0 : input items are terminated by null character instead of white spaces

xargs: unmatched single quote; by default quotes are special to xargs unless you use the -0 option
MS Dhoni & Sachin's Funny Moments At ISL Match With Bollywood Celebs-tokj3GJTwhE.mp4

 ls -a | grep -i 'dhoni ' | grep -v "'" |xargs -i echo "hey  {} hello"

ls -a | grep -i 'dhoni ' | grep -v "'" | xargs -i vlc {}

# Opens video 8 times

ls -a | grep -i 'dhoni ' | grep -v "'" | xargs -i mv {} dhoni/
# sucessfully moved files
# satisfying the particular grep condition

ls -a | grep -i 'dhoni ' | grep -v "'" | xargs -i echo {}

