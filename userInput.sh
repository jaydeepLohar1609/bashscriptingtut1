# Use read command for interactive user input in bash scripts. This is helpful for taking input from users at runtime.

echo "Enter your name:"
read myname
echo "Hello" $myname


# Let’s know some more options used with the read command. For example to prompt some message with the read command.

read -p "Enter your username: " myname1

# Use -s to input without displaying on the screen. This is helpful to take password input in the script.

read -sp "Enter your password: " mypassword1

echo -e "\nYour username is $myname and Password is $mypassword1"


