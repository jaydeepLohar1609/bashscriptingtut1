#  These are helpful to make a script more dynamic. Learn more about Bash command arguments.

#  $1 to $n	$1 is the first arguments, $2 is second argument till $n n’th arguments. From 10’th argument, you must need to inclose them in braces like ${10}, ${11} and so on
#  $0	The name of script itself
#  $$	Process id of current shell
#  $*	Values of all the arguments. All agruments are double quoted
#  $#	Total number of arguments passed to script
#  $@	Values of all the arguments
#  $?	Exit status id of last command
#  $!	Process id of last command
# A string with space should be enclosed in single or double quote

echo " The name of the Script $0"
echo " The value of the 1st cmd line arg $1 ------- 2nd cmd line arg $2 ------ 3rd cmd line ard $3"
echo " Process Id of the current Shello $$"
echo " Value of all argument in double quotes $*"
echo " Total number of cmd line argument passed to the script $#"
echo " Values of all the arguments $@"
echo " Exit status id of last command $? "
echo " Process id of last command $!"

args=("$@")
echo "args array " ${args}
echo "1st ele in args array is " ${args[0]}
echo "2st ele in args array is " ${args[1]}
echo "3st ele in args array is " ${args[2]}

