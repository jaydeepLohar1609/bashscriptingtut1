# The exit code is a number between 0 and 255. This is the value returns to parent process after completion of a child process. In other words, it denotes the exit status of the last command our function.

# The exit code value return based on a command or program will successfully execute or not.

# Success – A zero (0) value represents success.
# failure – A non-zero exit-code represents failure.


echo "hi" > ./tmp/tesfile.txt
 

# $? returns the execution status code of that always holds the return/exit code of the last executed command. You can view it in a terminal by running echo $? 
if [ $? -eq 0 ]; then
  echo "Hurrey. it works"
else
  echo "Sorry, can't write /tmp/tesfile.txt"
fi


echo "=========================================================="
STRING="tecadmin"
STRING1="whoopsie"
if grep ${STRING1} /etc/passwd
then
  echo "Exits Status code of previos command $? Yeah! string found"
else
  echo "Exits Status code of previos command $? Ooooh, no matching string found"
fi