read -p "Enter numeric value: " n1
read -p "Enter numeric value: " n2
 
echo "Addition of $n1 + $n2 is       = " $((n1+n2))
echo "Subtraction of $n1 - $n2 is    = " $((n1-n2))
echo "Division of $n1 / $n2 is       = " $((n1/n2))
echo "Multiplication of $n1 * $n2 is = " $((n1*n2))
echo "Modulus of $n1 % $n2 is        = " $((n1%n2))


# Increment and Decrement Operator:
# Bash also used incremnt (++) and decrement (–) operators. Both uses in two types pre-increment/post-increment and pre-decrement/post-decrement.


## Post-increment example 
var=10
echo $((var++))  ## First print 10 then increase value by 1
 
## Pre-increment example
var=10
echo $((++var))  ## First increase value by 1 then print 11 


#================================================================

## Post-decrement example 
var=10
echo $((var--))  ## First print 10 then decrease value by 1
 
## Pre-decrement example
var=10
echo $((--var))  ## First decrease value by 1 then print 9