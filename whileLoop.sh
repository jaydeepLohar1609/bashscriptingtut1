# condition is checked before executing while loop.



num=1
while [ $num -le 10 ]
do
   echo "$num"
   let num++
done

# Infinite while loop

# while true
# do
#   echo "Press CTRL+C to Exit"
# done


# exiting loop when  a condition satisfies
echo "printing num"$num
# (($num <= 30))
# while true
# do
#    if (($num >= 30));then
#       exit
#    fi
#    echo "$num"
#    ((num++))
# done

# C Style while loop
num2=1
while((num2 <= 15))
do
   echo $num2
   let num2++
done

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
# This is a useful feature provided by while loop to read file content line by line. Using this we can read file line by line and perform some tasks.

while read myvar
do
   echo $myvar
done < ./tmp/fileName.txt

# The while loop reads one line from the file in one iteration and assigned the value to the variable myvar.


