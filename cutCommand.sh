#  It can be used to cut parts of a line by byte position, character and field. Basically the cut command slices a line and extracts the text.
# jaydeep+ 23548  0.0  0.0  21536  1076 pts/6    S+   23:37   0:00 grep --color=auto tomcat


#  field slicing ..
val=$(ps aux | grep "tomcat" | head -n 1 | cut -d  ' ' -f 2)
printf "=======================$val=========\n", 


val1=$(ps aux | grep "tomcat" | head -n 1 | cut -d  ' ' -f 3-7)
printf "=======================$val1=========\n", 


val2=$(ps aux | grep "tomcat" | head -n 1 | cut -d  ' ' -f -7)
printf "=====================$val2============\n"

val3=$(ps aux | grep "tomcat" | head -n 1 | cut -d  ' ' -f 1-)
printf "======================$val3===============\n"

# =======================23059=========
# ,======================= 0.0  0.0 =========
# ,=====================jaydeep+ 23069  0.0  0.0 ============
# ======================jaydeep+ 23074  0.0  0.0  21536  1012 pts/6    S+   23:30   0:00 grep tomcat===============
# jaydeep+ 23548  0.0  0.0  21536  1076 pts/6    S+   23:37   0:00 grep --color=auto tomcat



# byte slicing.....
# $ cut -b 1,2,3 state.txt
#  cut -b 1-3,5-7 state.txt
ele1=$(ps aux | grep "tomcat" | head -n 1 | cut -b  2)
printf "=======================$ele1=========\n", 



ele2=$(ps aux | grep "tomcat" | head -n 1 | cut -b  2-7)
printf "=======================$ele2=========\n", 



ele3=$(ps aux | grep "tomcat" | head -n 1 | cut -b  -8)
printf "=======================$ele3=========\n", 



ele4=$(ps aux | grep "tomcat" | head -n 1 | cut -b  1-)
printf "=======================$ele4=========\n", 

# =======================a=========
# ,=======================aydeep=========
# ,=======================jaydeep+=========
# ,=======================jaydeep+ 23633  0.0  0.0  21536  1016 pts/6    S+   23:40   0:00 grep tomcat=========
# jaydeep+ 23548  0.0  0.0  21536  1076 pts/6    S+   23:37   0:00 grep --color=auto tomcat


# --------------------------------------------------------------------------------------------------
# character slicing.....
# $ cut -b 1,2,3 state.txt
#  cut -b 1-3,5-7 state.txt
# cut -c 1- state.txt
# cut -c -5 state.txt


chr1=$(ps aux | grep "tomcat" | head -n 1 | cut -c  2)
printf "=======================$chr1=========\n", 


chr2=$(ps aux | grep "tomcat" | head -n 1 | cut -c  2-7)
printf "=======================$chr2=========\n", 


chr3=$(ps aux | grep "tomcat" | head -n 1 | cut -c -8)
printf "=======================$chr3=========\n", 


chr4=$(ps aux | grep "tomcat" | head -n 1 | cut -c  1-)
printf "=======================$chr4=========\n", 

# ,=======================a=========
# ,=======================aydeep=========
# ,=======================jaydeep+=========
# ,=======================jaydeep+ 23934  0.0  0.0  21536   996 pts/6    S+   23:42   0:00 grep tomcat=========
# 
