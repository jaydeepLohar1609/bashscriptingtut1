function add_to_list
{
    thelist="$thelist $$"
}

for i in $(seq 1 100); do
    add_to_list&
done

wait
echo "The list: $thelist"


# Bash does not support threads. Only subprocesses. And it is impossible to change a variable of parent process in a subprocess.


function create_table
{
  sqlite3 thelist <<-%
  drop table t;
  create table t(one varchar(255));
  %
}

function add_to_list
{
  sqlite3 thelist <<-%
  .timeout 60000
  insert into t values('$$');
  %
}

function dump_list
{
  sqlite3 thelist <<-%
  select * from t;
  %
}

create_table
for i in $(seq 1 100); do
    add_to_list&
done

wait
thelist="$(dump_list)"
echo "The list: $thelist"

# 've been using Environment variables for ages but only just read about Environment variables vs. Shell variables. According to the definition:

# Environment variables are "system-wide" and inherited by child shells and processes
# Shell variables are only valid in the shell in which they are set
# The book I'm reading goes on to state that using export takes a variable in your current environment (bash shell) and makes it available in any every environment until you change it again.

# But if I change an Environment variable (HISTSIZE, for example) by calling export HISTZISE=999, and then check the value in a different terminal tab (echo $HISTSIZE), the change isn't visible. Why is this if the change is supposed to be system-wide? Personally I'm used to always setting my Environment variables when opening a new terminal tab, and I had taken it for granted that exporting values wouldn't affect other tabs (shells?).

# So my question is: Are different terminal tabs considered different shell environments? And what does "system-wide" then mean?

# Thanks for the help!

#-----------------------------------------------------------------------------------------------------------------

# export exports a variable to all children of the current shell.

# So if you do

# somevariable=somevalue
# export somevariable
# bash 
# echo $somevariable
# you'll see the value of $somevariable in this new shell.

# However shells in other terminal tabs are not children of the shell in the first tab, so they won't inherit the exported variables.

# The shells in terminal tabs are all children of the gnome process that opened the terminal, so they are "brothers and sisters". You can verify this by calling ps -f in two terminal tabs and looking at the column PPID (parent process ID) of the bash line. In my example both have the parent 5319 which is the gnome terminal process.

# tab 1:

# $ ps -f
# UID        PID  PPID  C STIME TTY          TIME CMD
# jean      5329  5319  0 10:36 pts/0    00:00:00 bash
# jean      5359  5329  0 10:37 pts/0    00:00:00 ps -f
# tab 2:

# $ ps -f
# UID        PID  PPID  C STIME TTY          TIME CMD
# jean      5363  5319  0 10:37 pts/1    00:00:00 bash
# jean      5372  5363  0 10:37 pts/1    00:00:00 ps -f

# $ ps -f -p5319
# UID        PID  PPID  C STIME TTY          TIME CMD
# jean      5319  2299  0 10:36 ?        00:00:02 /usr/lib/gnome-terminal/gnome-t


#-------------------------------------------------------------------------------------------------------------------

# Suppose I have export MY_VAR=0 in ~/.bashrc.

# That's your mistake right there. You should define your environment variables in ~/.profile, which is read when you log in. ~/.bashrc is read each time you start a shell; when you start the inner shell, it overrides MY_VAR. If you hadn't done that, your environment variable would propagate downwards.

# For more information on ~/.bashrc vs ~/.profile, see my previous posts on this topic.

# Note that upward propagation (getting a modified value from the subshell automatically reflected in the parent shell) is not possible, full stop.

#---------------------------------------------------------------------------------------------------------------------

# Suppose I have

# export MY_VAR=0
# in ~/.bashrc.

# I have an opened gnome terminal, and in this terminal, I change $MY_VAR value to 200. So, if I do

# echo $MY_VAR
# in this terminal, 200 is shown.

# Now, I opened another tab in my gnome terminal, and do

# echo $MY_VAR
# ...and instead of 200, I have 0.

# What should I do to persist the 200 value when a terminal modifies an environment variable, making this modification (setting to 200) available to all subsequent sub shells and such? Is this possible?


# A copy of the environment propagates to sub-shells, so this works:

# $ export MY_VAR=200
# $ bash
# $ echo $MY_VAR
# 200
# but since it's a copy, you can't get that value up to the parent shell — not by changing the environment, at least.

# It sounds like you actually want to go a step further, which is to make something which acts like a global variable, shared by "sibling" shells initiated separately from the parent — like your new tab in Gnome Terminal.

# Mostly, the answer is "you can't, because environment variables don't work that way". However, there's another answer, which is, well, you can always hack something up. One approach would be to write the value of the variable to a file, like ~/.myvar, and then include that in ~/.bashrc. Then, each new shell will start with the value read from that file.

# You could go a step further — make ~/.myvar be in the format MYVAR=200, and then set PROMPT_COMMAND=source ~/.myvar, which would cause the value to be re-read every time you get a new prompt. It's still not quite a shared global variable, but it's starting to act like it. It won't activate until a prompt comes back, though, which depending on what you're trying to do could be a serious limitation.

# And then, of course, the next thing is to automatically write changes to ~/.myvar. That gets a little more complicated, and I'm going to stop at this point, because really, environment variables were not meant to be an inter-shell communication mechanism, and it's better to just find another way to do it.