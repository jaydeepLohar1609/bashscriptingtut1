# If condition
read -p "Enter numeric value: " myvar
 
if [ $myvar -gt 10 ]
then
    echo "Value is greater than 10"
fi
echo"================================================================================="

# If else condition
read -p "Enter numeric value: " myvar
 
if [ $myvar -gt 10 ]
then
    echo "OK"
else
    echo "Not OK"
fi
echo"================================================================================="

#if elif else condition
read -p "Enter your marks: " marks
 
if [ $marks -ge 80 ]
then
    echo "Very Good"
 
elif [ $marks -ge 50 ]
then
    echo "Good"
 
elif [ $marks -ge 33 ]
then
    echo "Just Satisfactory"
else
    echo "Not OK"
fi

echo"================================================================================="
#nested if condition
read -p "Enter value of i :" i
read -p "Enter value of j :" j
read -p "Enter value of k :" k
 
if [ $i -gt $j ]
then
    if [ $i -gt $k ]
    then
        echo "i is greatest"
    else
        echo "k is greatest"
    fi
else
    if [ $j -gt $k ]
    then
        echo "j is greatest"
    else
 echo "k is greatest"
    fi
fi