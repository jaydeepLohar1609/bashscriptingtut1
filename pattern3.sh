#    *
#   *A*
#  *A*A*
# *A*A*A*

pattern3()
{
    pyramidHeight=$1
    for((i=1;i<=$pyramidHeight;i++))
    do
        # echo "--------$(($pyramidHeight-$i))"
        for((j=1;j<=$(($pyramidHeight-$i));j++))
        do
            printf " "
        done
        for((k=1;k<=$i;k++))
        do
            printf "*A"
        done
        printf "*\n"
    done
}
read -p "Enter the pyramid height " num
pattern3 $num