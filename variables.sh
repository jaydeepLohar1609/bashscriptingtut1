
name='Bash_Tutorials'
echo $name
echo $ENV #bash system variable
echo $PATH
# Global , local variable

globalVar='Global_Var'
function hello
{
    echo 'inside hello Function'
    local localVar="local variable value"
    echo $localVar
    echo $globalVar
}
hello
echo $localVar #as is is not delared globally so prints blank space
echo $globalVar


: '
    multipe lime
    comment
'

# OR

<< ANYSTRING
    multipe lime
    comment
ANYSTRING