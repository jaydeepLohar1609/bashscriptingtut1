# # read -p "Enter directory full path which contains mp4 files :-   " mp4SourcePath
# # if [ -d $mp4SourcePath ]
# # then
# #     echo "----------> thanks for entering a valid directory name <-----------"
# # else
# #     echo "---------->Enter a valid directory name <-----------"
# #     exit 8
# # fi
# # MP4FILE=$(ls $mp4SourcePath/ |grep '.mp4\|.webm')
# for filename in $mp4SourcePath
# do 
#     # echo $filename
#     # ls -l $filename
#     name=`echo "$filename" | sed -r 's/.{6}$//'`
#     echo " New mp3 file name would be $name.mp3"
#     # ffmpeg -i ~/Music/$filename -b:a 192K -vn ~/Music/$name.mp3
# done
# exit 0

# -----------> Data is on the server (mp4) and convert files are send via adb to client android mobile <--------------
if [[ ! -d mp3ConvertedSong ]]
then
    echo "--> mp3ConvertedSong does not exist so creating it <--------"
    mkdir ./mp3ConvertedSong
else
    echo "--> mp3ConvertedSong already exist so cleaning it <--------"
    rm -r mp3ConvertedSong/*
fi
for filename in *
do 
    # if [ -f "$filename" && $(echo $($filename | cut -c -3) = "mp4") ]
    if [ -f "$filename"  ] 
    then
        # if [[ grep -q ".mp4" <<< "$filename" || grep -q ".webm" <<< "$filename" ]];
        if [[ "$filename" == *"mp4"* || "$filename" == *"webm"* ]]
        then
            # echo "$filename"
            # ls -l $filename
            # echo "hello"
            name=`echo "$filename" | sed -r 's/.{6}$//'`
            # printf " New mp3 file name would be \n $name.mp3 \n"
            # stringName=$(echo "$filename" | sed -r 's/ /\ /' )
            # stringName=$( $filename | rename 's/ /_/g')
            mv "$filename" "${filename// /_}"
            # echo $filename
            ffmpeg -i "$filename" -b:a 192K -vn ./mp3ConvertedSong/$name.mp3
            # ffmpeg -i "$filename" -b:a 192K -vn ./sdcard/$name.mp3
            
        fi
    fi
done
read -p "Want to push a copy of the coverted file to remote adb connected mobile :- " choice
if [[ "$choice" == *"y"* ]]
    then
    adb shell "sleep 1; cd /sdcard/; ls -l | grep mp3ConvertedSong"
    if [[ ! $? == 0 ]]
    then
        echo "--> mp3ConvertedSong does not exist on remote adb so creating the same :- <--"
        adb shell "cd /sdcard/; mkdir mp3ConvertedSong"
    fi
    echo "--> mp3ConvertedSong already exist :- <--"
    adb push mp3ConvertedSong/* /sdcard/mp3ConvertedSong/
fi
exit 0