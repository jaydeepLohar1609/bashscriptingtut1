openNewTabAndStartServer()
{
    echo "==== in open new tab function"
    # pidOfRunningPwaServer=$(lsof -t -i:3000)    
    kill -9 $(lsof -t -i:3000)
    if [ $? == 0 ]
    then
        echo "sucessfully killed running process on port 3000"
    fi
    gnome-terminal --tab='BatteryDekho' -e "bash -c 'sleep 2  && $1 > $2;exec $SHELL'"
    # gnome-terminal --tab='BatteryDekho' -e "bash -c 'sleep 2 && kill -9 $(lsof -t -i:3000) ; $1 > $2;exec $SHELL'"
    # exprssionToExecuteInTerminal="$(echo 'in sub condition' && sleep 2 && kill $pidOfRunningPwaServer ; npm run start-dev > ../admin/logs.txt)"
    # gnome-terminal --tab='BatteryDekho' -e "$(eckho 'in sub condition' && sleep 2 && kill $pidOfRunningPwaServer ; npm run start-dev > ../admin/logs.txt)"
    if [ $? == 0 ]
    then
        echo "started pwa server with command $1"
        parentTerminalProcessId=$(pgrep gnome-terminal)
        # tomcatPid=$(ps aux|grep tomcat | head -n 1 | cut -d ' ' -f 2) && [ ${#tomcatPid} = 0 ] && echo "length is zero" && tomcatPid=$(ps aux|grep tomcat | head -n 1 | cut -d ' ' -f 3) || echo "length is not Zero"
        newTabPid=$(ps --ppid $parentTerminalProcessId | tail -1 | cut -d ' ' -f 1) && [ ${#newTabPid} = 0 ] && echo "---> length is zero" && newTabPid=$(ps aux|grep tomcat | head -n 1 | cut -d ' ' -f 2) || echo "-----> length is not Zero"
        echo "============== new tab openend with pid -----> $newTabPid <--------"
        readServerLogsSleepStatic $2 $newTabPid
    else
        echo "error occured while opening new tab"
    fi
}

# cat -v terminalOperation_V2.sh
checkExpressionMatched()
{
    head -n 20 $1 | grep 'http://localhost:3000'
    PID=$$
    echo "---------------- grep process pid -----> $PID <-------"
    while [ -e /proc/$PID ]
    do
        echo "Process: $PID is still running" >> "../admin/searchLogs.txt"
        sleep .6
    done
    echo "Process $PID has finished" >> "../admin/searchLogs.txt"
}
readServerLogsSleepStatic()
{
    echo "------> read server logs 2 of  $1"
    sleep 8
    head -n 20 $1 | grep 'http://localhost:3000'
    if [ $? == 0 ]
    then
        echo " localhost:3000 exp matched , so opening browser"
        openNewTabInBrowser 3000
    else
        echo " localhost:3000  exp doesn't matched & server not started so killing new tab process with pid ------------> $2 <------------"
        sleep 3 && kill -9 $2
    fi  
}
readServerLogs()
{
    echo "------> read server logs 2 of  $1"
    sleep 6
    # first20Lines=$(head -n 20 $1)
    # $(("$first20Lines" | grep 'dev'))
    # wait $$
    # line 24: wait: pid 5591 is not a child of this shell
    # head -n 20 $1 | grep 'http://localhost:3000'
    checkExpressionMatched $1
    if [ $? == 0 ]
    then
        echo " localhost:3000 exp matched"
        # openNewTabInBrowser 3000
    else
        echo " localhost:3000  exp doesn't matched & server not started so killing process"
        sleep 3 && kill -9 $2
    fi
}
openNewTabInBrowser()
{
    echo "opening new tab in browser with port $1"
    xdg-open http:localhost:$1 && exit
}
echo "===========bash script called"
logsPath="./logs.txt"
serverUpCommand="npm run start-dev"
openNewTabAndStartServer "npm run start-dev"  $logsPath
# exit
