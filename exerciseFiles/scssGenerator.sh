scssGeneratorFunction()
{
    echo "----------------------------- $1   ------- $2   ----------- $3"
    $1 $2 $3
    if [ $? == 0 ]
    then
        echo "-----------------> scss generated successfully <--------------------------------"
        if [ $fileReadCount == 1 ]
        then 
            echo "------------> running npm install --------------->" &&  npm i &&   echo "------------> node dependencies installed successfully --------------->"
        fi
        echo "--------------------------> Starting Gulp Build Local <---------------------------------------------"
        runGulpBuildAndStartLocal "npm run gulp-default:local" "npm run build:local"

    else
        echo "-----------------> erro occured while scss generating"
    fi
}

runGulpBuildAndStartLocal()
{
    $1 
    if [ $? == 0 ]
    then
         echo "--------------------------------> gulp run successfully <---------------------"
    else
        echo "--------------------------------> gulp cmd raised error <-----------------------"
        exit
    fi
    $2
    if [ $? == 0 ]
    then
         echo "--------------------------------> build run successfully <----------------------------------"
    else
        echo "--------------------------------> build cmd raised error <---------------------------"
        exit
    fi    
    kill -9 $(lsof -t -i:3000)
    if [ $? == 0 ]
    then
        echo "-----------------------> sucessfully killed running process on port 3000 <----------------------------"
    fi
    startLocal  "npm run start:local"
}

readServerLogsSleepStatic()
{
    echo "------> read server logs of  $1"
    sleep 8
    head -n 20 $1 | grep 'http://localhost:3000'
    if [ $? == 0 ]
    then
        echo " localhost:3000 exp matched"
        if [  $fileReadCount == 2 ]
        then
            openNewTabInBrowser 3000
        fi
    else
        echo " localhost:3000  exp doesn't matched & server not started so killing new tab process with pid ---------------> $2 <--------------"
        sleep 3 && kill -9 $2
    fi  
}

openNewTabInBrowser()
{
    echo "opening new tab in browser with port $1"
    xdg-open http:localhost:$1 && exit
}

startLocal()
{
    logsPath="./logs.txt"
    gnome-terminal --tab='TyreDekho' -e "bash -c 'sleep 2  && $1 > $logsPath;exec $SHELL'"
    if [ $? == 0 ]
    then
        echo "started pwa server with command $1"
        parentTerminalProcessId=$(pgrep gnome-terminal)
        newTabPid=$(ps --ppid $parentTerminalProcessId | tail -1 | cut -d' ' -f1)
        echo "============== new tab openend with pid -----> $newTabPid <--------"
        readServerLogsSleepStatic $logsPath $newTabPid
    else
        echo "-------------------> error occured while opening new tab <----------------------------------------"
    fi
}
fileReadCount=1
echo "-----------------> bash Script Called with name $0 and project name passed is $1 <----------------------"
compiler="./node_modules/.bin/babel-node"
scssGeneratorFilePath="jobs/generators/scssGenerator.js"
criticalCssGeneratorFilePath="jobs/generators/criticalGenerator.js"
echo "========================= scss generator started ============================="
scssGeneratorFunction   $compiler $scssGeneratorFilePath $1
echo "========================= scss generated successfully with gulp, build & start local command ============================="
$((++fileReadCount))
echo "========================= critical css generator started ============================="
scssGeneratorFunction  $compiler $criticalCssGeneratorFilePath $1
echo "========================= critical generated successfully with gulp, build & start local command ============================="
