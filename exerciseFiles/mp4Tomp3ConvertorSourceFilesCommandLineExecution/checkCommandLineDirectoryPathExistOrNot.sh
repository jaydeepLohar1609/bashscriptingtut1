checkCommandLineDirectoryPathExistOrNot(){
    echo " inside check command line path validating $1"
     if [[ ! -d $1 ]]
        then    
            echo "--> inputted command line path $1 is not a directory <---"
            exit 11
        fi
        cd $1
        echo " --> changed pwd to $1 and its content is as following :- --->"
        ls -la
}
mp4FolderPath="/sdcard/Download"
checkCommandLineDirectoryPathExistOrNot $mp4FolderPath