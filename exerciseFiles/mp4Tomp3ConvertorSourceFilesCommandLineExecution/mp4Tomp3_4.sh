# -----------> Script is on the server ,Input  (mp4) are pushed via adb from client and converted mp3 Output files are send via adb to client android mobile <--------------
# -----------> Script is on the server ,Input  (mp4) directory path passed as command line while executing script on server are pushed via adb from client and converted mp3 Output files are send via adb to client android mobile <--------------


# As /system/bin/sh: ffmpeg: not found ffmpeg module is not adb shell
# So first pulling mp4 data
# Converting mp4 -> mp3
# Pusing path again the mp3 file with whole Directory
# And finally cleaning all unwanted data (temporary"
#  
# source checkCommandLineDirectoryPathExistOrNot.sh

createDirectoryIfNotExistElseCleanIt(){
    if [[ ! -d $1 ]]
    then
        echo "--> $1 does not exist so creating it <--------"
        mkdir $1
    else
        echo "--> $1 already exist so cleaning it <--------"
        ls -l $1/ | grep "total 0"  && echo " ---> Directory is alread empty" || rm -r $1/*
    fi
}

startAdbPull(){
    echo " Pulling mp4 files from Android to Server for Conversion process"
    adb pull $1 ./mp4Files
    if [[ $? == 0 ]]
    then
        echo " mp4 Files pulled successfully ----"
        cd ./mp4Files
        startNameConversionProcess
    else
        echo " some error occured while pulling mp4 files"
        exit 9
    fi
}

startAdbPush(){
    echo " Pushing converted mp3 files from Server to Client Android"
    adb push ../mp3ConvertedSong_1 $1
    if [[ $? == 0 ]]
    then
        echo " mp3 Files with directory pushed successfully ----"
    else
        echo " some error occured while pushing mp3 files"
        exit 5
    fi
}

startNameConversionProcess(){
    for filename in *
    do 
        if [ -f "$filename"  ] 
        then
            if [[ "$filename" == *"mp4"* || "$filename" == *"webm"* ]]
            then
                # echo "$filename"
                # ls -l $filename
                # echo "hello"
                # name=`echo "$filename" | sed -r 's/.{6}$//'`
                # printf " New mp3 file name would be \n $name.mp3 \n"
                # stringName=$(echo "$filename" | sed -r 's/ /\ /' )
                # stringName=$( $filename | rename 's/ /_/g')
                mv "$filename" "${filename// /_}"
                # echo $filename
                # updatedFileName=${filename// /_}
                # ffmpeg -i "$updatedFileName" -b:a 320K -vn ../../mp3ConvertedSong_1/$name.mp3
                # ffmpeg -i "$filename" -b:a 192K -vn ./sdcard/$name.mp3
                
            fi
        fi
    done
    startConversionProcess
}

startConversionProcess(){
    for filename in *
    do 
        # if [ -f "$filename" && $(echo $($filename | cut -c -3) = "mp4") ]
        if [ -f "$filename"  ] 
        then
            # if [[ grep -q ".mp4" <<< "$filename" || grep -q ".webm" <<< "$filename" ]];
            if [[ "$filename" == *"mp4"* || "$filename" == *"webm"* ]]
            then
                name=`echo "$filename" | sed -r 's/.{6}$//'`
                ffmpeg -i "$filename" -b:a 320K -vn ../../mp3ConvertedSong_1/$name.mp3                
            fi
        fi
    done
}

adbConnection(){
    adb shell "exit"
    if [[  $? == 0 ]]
    then
        echo " Android device is connected "
    #     printf " Adb Android Client is not connected So exiting the script \n"
    #     # echo " Trying to connect using ip please enter ip"
    #     echo " Trying to connect using command line inputted ip"
    #     # read androidClientIp
    #     echo "$2"
    #     adb connect $2
    #     adb shell "echo 'Connected to ADB using Wifi ----> '"
    #     if [[ ! $? == 0 ]]
    #     then
    #         printf " No android client on the specified ip exiting the script"
    #         exit 3
    #     fi
    else
        echo " PLease connect the android device "
        exit 19
    fi
}



# Script starts command
adbConnection
cd ~/Desktop/
# ls -l
createDirectoryIfNotExistElseCleanIt "mp4Tomp3ConversionProcessDirectory"
createDirectoryIfNotExistElseCleanIt "mp3ConvertedSong_1"

adb shell "$(cat ~/Documents/bashscriptingtut1/exerciseFiles/mp4Tomp3ConvertorSourceFilesCommandLineExecution/checkCommandLineDirectoryPathExistOrNot.sh)"
echo " now startin the process cd -> mp4Tomp3ConversionProcessDirectory"
cd ./mp4Tomp3ConversionProcessDirectory
startAdbPull $1
# startConversionProcess
if [[ $? == 0 ]]
then
    echo "-------> All files has been converted and ready to push  to the client (Android) -->" 
    cd ../
    startAdbPush $1
else
    echo "---> Some error ocurred in the process --->"
    exit 15
fi


# read -p "Want to push a copy of the coverted file to remote adb connected mobile :- " choice
# choice=y
# if [[ "$choice" == *"y"* ]]
#     then
#     # adb shell "sleep 1; cd /sdcard/; ls -l | grep mp3ConvertedSong_1"
#     if [[ ! $? == 0 ]]
#     then
#         echo "--> mp3ConvertedSong_1 does not exist on remote adb so creating the same :- <--"
#         # adb shell "cd /sdcard/; mkdir mp3ConvertedSong_1"
#     fi
#     echo "--> mp3ConvertedSong_1 already exist :- <--"
#     # adb push mp3ConvertedSong_1/* /sdcard/mp3ConvertedSong_1/
# fi
exit 0