cleanLogsFile(){
    if [[ -d $tomcatDirectoryPath/logs ]]
    then
        rm -r $tomcatDirectoryPath/logs/* && echo " ---> expired logs are deleted <-- "
        rm $tomcatDirectoryPath/bin/logger.log && echo " ---> logger.log file also deleted <-- "
    else
        echo "--> $tomcatDirectoryPath/logs directory does not exist"
        rm $tomcatDirectoryPath/bin/logger.log && echo " ---> logger.log file also deleted <-- "
    fi
}

killServer(){
    if [[ `ps aux | grep $1 | wc -l` -gt 1  ]]
    then
        newTabPid=$(ps aux | grep $1 | head -n 1 | cut -d ' ' -f 2) && [ ${#newTabPid} = 0 ] && echo "---> length is zero" && newTabPid=$(ps aux | grep tomcat | head -n 1 | cut -d ' ' -f 3) && echo "---> length was zero but now it is not <-- " || echo "-----> length is not Zero"
        echo " ---> Process Id of Tomcat Server is $newTabPid <--- "  && sleep 8
        [ ! ${#newTabPid} = 0 ] && kill -9 $newTabPid && echo "--> tomcat server was stopped and resources are free <-- " && sleep 8
    else
        echo " ---> There is not process actively running with the named as specified $1"
    fi
}

StartTomcatServer(){
    if [[ -d $tomcatDirectoryPath ]]
    then
        cd $tomcatDirectoryPath/bin && sh catalina.sh jpda start && echo " --> tomcat server started in debugger mode <-- "
    else
        echo "--> $tomcatDirectoryPath directory does not exist"
    fi
}

tailLogFilesOnTerminal(){
    if [[ -d $tomcatDirectoryPath/logs && -f $tomcatDirectoryPath/bin/logger.log ]]
    then
        tail -f $tomcatDirectoryPath/bin/logger.log  $tomcatDirectoryPath/logs/catalina.$currentDate.log   $tomcatDirectoryPath/logs/host-manager.$currentDate.log   $tomcatDirectoryPath/logs/localhost_access_log.$currentDate.txt   $tomcatDirectoryPath/logs/catalina.out   $tomcatDirectoryPath/logs/localhost.$currentDate.log  $tomcatDirectoryPath/logs/manager.$currentDate.log
        # rm -r $tomcatDirectoryPath/logs/* && echo " ---> expired logs are deleted <-- "
        # rm $tomcatDirectoryPath/bin/logger.log && echo " ---> logger.log file also deleted <-- "
    else
        echo "--> $tomcatDirectoryPath/logs directory does not exist"
        # rm $tomcatDirectoryPath/bin/logger.log && echo " ---> logger.log file also deleted <-- "
    fi
}

tomcatDirectoryPath="/home/jaydeeplohar/Documents/tomcat"
currentDate=`date +%Y-%m-%d`
# currentDate=$(date +%Y-%m-%d)


killServer 'tomcat-juli' #paasing process name as argument
cleanLogsFile
StartTomcatServer
sleep 8 # as logger.log will be created and can be read by tail command.
tailLogFilesOnTerminal


