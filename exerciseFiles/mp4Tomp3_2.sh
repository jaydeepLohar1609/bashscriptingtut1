# # read -p "Enter directory full path which contains mp4 files :-   " mp4SourcePath
# # if [ -d $mp4SourcePath ]
# # then
# #     echo "----------> thanks for entering a valid directory name <-----------"
# # else
# #     echo "---------->Enter a valid directory name <-----------"
# #     exit 8
# # fi
# # MP4FILE=$(ls $mp4SourcePath/ |grep '.mp4\|.webm')
# for filename in $mp4SourcePath
# do 
#     # echo $filename
#     # ls -l $filename
#     name=`echo "$filename" | sed -r 's/.{6}$//'`
#     echo " New mp3 file name would be $name.mp3"
#     # ffmpeg -i ~/Music/$filename -b:a 192K -vn ~/Music/$name.mp3
# done
# exit 0

# -----------> Script is on the server ,Input  (mp4) are pushed via adb from client and converted mp3 Output files are send via adb to client android mobile <--------------
# -----------> Script is on the server ,Input  (mp4) directory path passed as command line while executing script on server are pushed via adb from client and converted mp3 Output files are send via adb to client android mobile <--------------


# As /system/bin/sh: ffmpeg: not found ffmpeg module is not adb shell
# So first pulling mp4 data
# Converting mp4 -> mp3
# Pusing path again the mp3 file with whole Directory
# And finally cleaning all unwanted data (temporary"
#  

cd /sdcard/
ls -l
if [[ ! -d mp3ConvertedSong_1 ]]
then
    echo "--> mp3ConvertedSong_1 does not exist so creating it <--------"
    mkdir ./mp3ConvertedSong_1
else
    echo "--> mp3ConvertedSong_1 already exist so cleaning it <--------"
    ls -l mp3ConvertedSong_1/ | grep "total 0"  && echo " ---> Directory is alread empty" || rm -r mp3ConvertedSong_1/*
fi
# echo "--> changing cd to $1 <--- "
# if [[ ! -d $1 ]]
# then    
#     echo "--> inputted command line path $1 is not a directory <---"
#     exit 11
# fi
# cd $1
# echo " --> changed pwd to $1 and staring process of conversion --->"

mp4FolderPath="/sdcard/mp4Files"
echo "--> changing cd to $mp4FolderPath <--- "
if [[ ! -d $mp4FolderPath ]]
then    
    echo "--> inputted command line path $mp4FolderPath is not a directory <---"
    exit 11
fi
cd $mp4FolderPath
echo " --> changed pwd to $mp4FolderPath and staring process of conversion --->"

for filename in *
do 
    # if [ -f "$filename" && $(echo $($filename | cut -c -3) = "mp4") ]
    if [ -f "$filename"  ] 
    then
        # if [[ grep -q ".mp4" <<< "$filename" || grep -q ".webm" <<< "$filename" ]];
        if [[ "$filename" == *"mp4"* || "$filename" == *"webm"* ]]
        then
            # echo "$filename"
            # ls -l $filename
            # echo "hello"
            name=`echo "$filename" | sed -r 's/.{6}$//'`
            # printf " New mp3 file name would be \n $name.mp3 \n"
            # stringName=$(echo "$filename" | sed -r 's/ /\ /' )
            # stringName=$( $filename | rename 's/ /_/g')
            mv "$filename" "${filename// /_}"
            # echo $filename
            ffmpeg -i "$filename" -b:a 192K -vn ./mp3ConvertedSong_1/$name.mp3
            # ffmpeg -i "$filename" -b:a 192K -vn ./sdcard/$name.mp3
            
        fi
    fi
done
# read -p "Want to push a copy of the coverted file to remote adb connected mobile :- " choice
# choice=y
# if [[ "$choice" == *"y"* ]]
#     then
#     # adb shell "sleep 1; cd /sdcard/; ls -l | grep mp3ConvertedSong_1"
#     if [[ ! $? == 0 ]]
#     then
#         echo "--> mp3ConvertedSong_1 does not exist on remote adb so creating the same :- <--"
#         # adb shell "cd /sdcard/; mkdir mp3ConvertedSong_1"
#     fi
#     echo "--> mp3ConvertedSong_1 already exist :- <--"
#     # adb push mp3ConvertedSong_1/* /sdcard/mp3ConvertedSong_1/
# fi
exit 0