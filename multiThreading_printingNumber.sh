# #!/bin/bash
# START=$(date +%s)
# # do something

# # start your script work here
# ls -R /etc > /tmp/x
# rm -f /tmp/x
# # your logic ends here

# END=$(date +%s)
# DIFF=$(( $END - $START ))
# echo "It took $DIFF seconds"

START=$(date +%s)
printNumberAscending()
{
    for ((i=1; i<=$1; i++))
    do
        printf " ++++++ $i ++++++++"
    done
    echo "============ Ascending function completed =============="
}
printNumberDescending()
{
    for ((i=$1; i>=1; i--))
    do
        printf " ------- $i --------"
    done
    echo "============ Descending function completed =============="
}
printNumberAscending 1000000 &
printNumberDescending 1000000
printNumberAscending 1000000
printNumberDescending 1000000
wait
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "It took -----> $DIFF <-------- seconds"

# It took -----> 28 <-------- seconds #Without multiThreading
# It took -----> 23 <-------- seconds #With multiThreading


# It took -----> 68 <-------- seconds #Without multiThreading
# It took -----> 54 <-------- seconds #With multiThreading